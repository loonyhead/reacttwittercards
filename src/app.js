const setup = require('./setup');
let app = setup.createExpressApp();

setup.createRoutes(app);
setup.handleExpressErrors(app);

app.listen(3000, () => {
    console.log("App listening to port 3000");
});

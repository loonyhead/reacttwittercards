import React from 'react';
import ReactDOM from 'react-dom';

import {createStore} from 'redux';
import {Provider} from 'react-redux';

import reducer from './reducer';

const store = createStore(reducer);

import Twitter from './twitter/twitter';

class App extends React.Component {
  render() {
    return (
      <div className="app">
        <Twitter />
      </div>
    )
  }
}


const AppWithStore = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  )
}

ReactDOM.render(<AppWithStore />, document.getElementById('app'));

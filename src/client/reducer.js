import {combineReducers} from 'redux';

import tweets from './twitter/twitter_reducer';

const reducer = combineReducers({
  tweets
});

export default reducer;

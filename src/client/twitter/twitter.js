import React from 'react';
import { connect } from 'react-redux';
import TwitterCard from './twitter_cards';
import twitterActions from './twitter_actions';

import './twitter.less';

class Twitter extends React.Component {
  componentDidMount() {
    this.loadTweets();

    //start a interval
    this.tweetIntrvl = setInterval(() => {
      this.loadTweets();
    }, 60 * 1000);
  }
  componentWillUnmount() {
    clearInterval(this.tweetIntrvl);
  }
  loadTweets() {
    const {dispatch} = this.props;
    twitterActions.loadTweets(dispatch);
  }
  renderCards() {
    const {tweets} = this.props;
    return tweets.map((tweet, idx) => {
      return <TwitterCard key={idx} tweet={tweet} />
    });
  }
  render() {
    const {tweets} = this.props;
    return (
      <div className="twitter-wrapper">
        {tweets.length ? this.renderCards() : <div>Loading....</div>}
      </div>
    )
  }
}

Twitter = connect((state) => {
  return {tweets: state.tweets}
}, (dispatch) => {
  return {
    dispatch: dispatch
  }
})(Twitter);

export default Twitter;

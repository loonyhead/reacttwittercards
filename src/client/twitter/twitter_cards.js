import React from 'react';
import './twitter_cards.less';

class TwitterCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "twitterCard" :"twitter-card",
      "showCardBack" :"hidden"
    };
  }
  flipCard(){
    this.state.showCardBack === "hidden"?this.setState({"showCardBack" :"flipped"}):this.setState({"showCardBack" :"hidden"});
  }
  render() {
    const {tweet} = this.props;
    return (
        <div className={[this.state.twitterCard, this.state.showCardBack].join(' ')} onClick={this.flipCard.bind(this)}>
          <div className="card-front">
            {tweet.name}
          </div>
          <div className="card-back">
            <img src={tweet.image} alt />
            <span className="sname">@{tweet.screen_name}</span>
            <span className="flrs">{tweet.followers} followers</span>
          </div>
        </div>
    );
  }
}

export default TwitterCard;

import $ from "jquery";

const twitterActions = {
  loadTweets : function(dispatch) {
    $.ajax({
      url : '/api/tweets/cricket',
      success: function(data) {
        dispatch({
          type: 'TAPP.TWITTER_LOAD',
          payload: {
            tweets: data.tweets
          }
        })
      }
    });
  }
}

export default twitterActions;

function tweets(state = [], action){
  switch (action.type) {
    case 'TAPP.TWITTER_LOAD':
      return action.payload.tweets;
      break;
    default:
      return state;
  }
}

export default tweets;

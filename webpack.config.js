var autoprefixer = require('autoprefixer');

module.exports = {
    devtool: process.env.NODE_ENV !== 'production' ? 'eval' : null,

    entry: {
      'bundle' : [
        './src/client/index.js'
      ]
    },
    devtool: "eval",
    debug: true,
    output: {
        path: './public',
        publicPath: '/',
        filename: '[name].js'
    },
    resolveLoader: {
      modulesDirectories: ['node_modules']
    },
    resolve: {
      extensions: ['','.js']
    },
    postcss: function () {
        return [autoprefixer];
    },
    module: {
      loaders: [
        {
          test: /\.less|.css$/,
          loader: "style!raw!postcss!less"
        },
        {test: /\.js$/, loaders:['babel'], exclude: /node_modules/},
      ]
    }
};
